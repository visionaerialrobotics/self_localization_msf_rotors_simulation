##  Self localization MSF rotors simulation

In order to install and execute this project, perform the following steps:

### Install the project

- Download the installation files:

        $ git clone https://bitbucket.org/visionaerialrobotics/aerostack_installers.git ~/temp

- Run the following installation script to install the project "self_localization_msf_rotors_simulation":

        $ ~/temp/install_project_from_source.sh projects_test/self_localization_msf_rotors_simulation

- Remove "CATKIN_IGNORE" file located in the folder "stack/localization_and_mapping_system/eth_msf"

- Compile Aerostack

        $ cd $AEROSTACK_WORKSPACE
        $ catkin_make

### Execute the project

- Change directory to this project:

        $ cd $AEROSTACK_STACK/projects_test/self_localization_msf_rotors_simulation

- Execute the script that launches Gazebo:

        $ ./launcher_gazebo.sh

- Wait until the following window is presented:

![capture-gazebo.png](https://bitbucket.org/repo/rokr9B/images/916057309-capture-gazebo.png)

- Open a new terminal and change directory to the project:

        $ cd $AEROSTACK_STACK/projects_test/self_localization_msf_rotors_simulation

- Execute the script that launches the Aerostack components for this project:
 
        $ ./main_launcher.sh

- Execute the following command to run the mission:

        $ rosservice call /drone111/python_based_mission_interpreter_process/start

- To stop the processes execute the following script:

        $ ./stop.sh

- To close the inactive terminals:

        $ killall bash
